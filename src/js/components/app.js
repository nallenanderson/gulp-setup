import React from 'react';

import Box from './box';

export default class App extends React.Component {
  render(){
    return(
      <div className="container">
        <h1>Welcome to the Gulped App.</h1>
        <Box />
      </div>
    );
  }
}
